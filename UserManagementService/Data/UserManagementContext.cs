﻿using System;
using Microsoft.AspNetCore.Identity;
using Microsoft.AspNetCore.Identity.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata;
using Microsoft.Extensions.Logging;
using UserManagementService.Models;

namespace UserManagementService.Data
{
    public partial class UserManagementContext : IdentityDbContext<User>
    {
        private readonly ILogger Logger;

        public DbSet<ApiToken> ApiTokens { get; set; }
        public DbSet<Permission> Permissions { get; set; }

        public UserManagementContext(DbContextOptions<UserManagementContext> options, ILogger<UserManagementContext> logger) : base(options)
        {
            Logger = logger;
        }

        protected override void OnConfiguring(DbContextOptionsBuilder optionsBuilder)
        {
            if (!optionsBuilder.IsConfigured)
            {
                Logger.LogError("Database not correctly configured");
                throw new ApplicationException("Database not configured");
            }
        }

        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            base.OnModelCreating(modelBuilder);

            // Create primary keys for many-to-many glue tables
            //modelBuilder.Entity<GroupRole>().HasKey(e => new { e.GroupId, e.RoleId });
            //modelBuilder.Entity<UserGroup>().HasKey(e => new { e.UserId, e.GroupId });
            modelBuilder.Entity<ApiTokenPermission>().HasKey(e => new { e.Token, e.PermissionId });
            //modelBuilder.Entity<IdentityUserRole<Guid>>().HasKey(r => new { r.UserId, r.RoleId });
            //modelBuilder.Entity<IdentityUserClaim<Guid>>().HasKey(r => new { r.UserId, r.Id });


            // Connect many-to-many tables both ways
            //modelBuilder.Entity<GroupRole>()
            //    .HasOne(groupRole => groupRole.Group)
            //    .WithMany(group => group.GroupRoles)
            //    .HasForeignKey(groupRole => groupRole.GroupId);

            //modelBuilder.Entity<GroupRole>()
            //    .HasOne(groupRole => groupRole.Role)
            //    .WithMany(role => role.Groups)
            //    .HasForeignKey(groupRole => groupRole.RoleId);

            //modelBuilder.Entity<UserGroup>()
            //    .HasOne(userGroup => userGroup.User)
            //    .WithMany(user => user.UserGroups)
            //    .HasForeignKey(userGroup => userGroup.UserId);

            //modelBuilder.Entity<UserGroup>()
            //    .HasOne(userGroup => userGroup.Group)
            //    .WithMany(group => group.GroupUsers)
            //    .HasForeignKey(userGroup => userGroup.GroupId);

            modelBuilder.Entity<ApiTokenPermission>()
                .HasOne(tokenPermission => tokenPermission.ApiToken)
                .WithMany(token => token.Permissions)
                .HasForeignKey(tokenPermission => tokenPermission.Token);

            modelBuilder.Entity<ApiTokenPermission>()
                .HasOne(tokenPermission => tokenPermission.Permission)
                .WithMany(permission => permission.ApiTokens)
                .HasForeignKey(tokenPermissions => tokenPermissions.PermissionId);
        }
    }
}
