﻿using Microsoft.AspNetCore.Hosting;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Pomelo.EntityFrameworkCore.MySql;
using Microsoft.EntityFrameworkCore;
using UserManagementService.Models;
using Microsoft.Extensions.Configuration;
using Microsoft.AspNetCore.Identity;
using System.Security.Claims;

namespace UserManagementService.Data
{
    public static class DbInitialiser
    {
        public static async Task Initialise(UserManagementContext context,
            IHostingEnvironment env,
            IConfiguration configuration,
            UserManager<User> userManager,
            RoleManager<IdentityRole> roleManager)
        {
            context.Database.Migrate();

            var requiredRoles = new IdentityRole[] {
                new IdentityRole("CreateApiTokens"),
                new IdentityRole("ViewApiTokens"),
                new IdentityRole("InvalidateApiTokens"),
                new IdentityRole("CreateUsers"),
                new IdentityRole("ActivateUsers"),
                new IdentityRole("DeactivateUsers"),
                new IdentityRole("RemoveUsers"),
                new IdentityRole("GrantRoles"),
                new IdentityRole("ChangeActiveService"),};

            if (!roleManager.Roles.Any())
            {
                foreach (var role in requiredRoles)
                {
                    await roleManager.CreateAsync(role);
                }
            }

            if ((await userManager.FindByNameAsync("SuperUser")) == null)
            {
                var superUserPassword = configuration.GetValue<string>("DefaultSuperUserPassword");
                var superUser = new User("SuperUser");
                var success = await userManager.CreateAsync(superUser, superUserPassword);
                await userManager.AddToRolesAsync(superUser, requiredRoles.Select(role => role.Name));
            }

            if ((await userManager.FindByNameAsync("GhostUser")) == null)
            {
                var user = new User("GhostUser");
                await userManager.CreateAsync(user);
            }

            var ghostUser = await userManager.FindByNameAsync("GhostUser");
            ghostUser.LockoutEnabled = true;
            await userManager.UpdateAsync(ghostUser);

            //if (!context.Users.Any())
            //{
            //    // Create super user
            //    var superUserPassword = configuration.GetValue<string>("DefaultSuperUserPassword");
            //    var superUser = User.CreateUser("SuperUser", superUserPassword);

            //    // Grant all basic roles to super user
            //    foreach (var role in requiredRoles)
            //    {
            //        if (!superUser.UserRoles.Any(ur => ur.Role.RoleName == role.RoleName))
            //        {
            //            superUser.GrantRole(role);
            //        }
            //    }

            //    // Add super user and ghost user (used to substitude deleted users)
            //    context.AddRange(new User[]
            //    {
            //        superUser,
            //        User.CreateUser("GhostUser", "")
            //    });
            //    context.SaveChanges();
            //}

            //// Deactivate Ghost User if it's active
            //var ghostUser = context.Users.First(u => u.UserName == "GhostUser");
            //if (ghostUser.IsActive)
            //{
            //    ghostUser.IsActive = false;
            //    context.SaveChanges();
            //}

            //if (!context.Groups.Any())
            //{
            //    var superUserGroup = new Group { GroupName = "SuperUsers" };

            //    // Grant all basic roles to super user group
            //    foreach (var role in requiredRoles)
            //    {
            //        if (!superUserGroup.GroupRoles.Any(ur => ur.Role.RoleName == role.RoleName))
            //        {
            //            superUserGroup.GrantRole(role);
            //        }
            //    }

            //    var superUser = context.Users.FirstOrDefault(u => u.UserName == "SuperUser");
            //    if (superUser != null) superUser.GrantMembership(superUserGroup);

            //    context.Add(superUserGroup);
            //    context.SaveChanges();
            //}


        }
    }
}
