﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace UserManagementService.Models
{
    public class ApiToken
    {
        [MaxLength(64)]
        [Key]
        public string Token { get; set; }

        public DateTime LastUsed { get; set; }

        public bool Valid { get; set; }

        public virtual ICollection<ApiTokenPermission> Permissions { get; set; } = new List<ApiTokenPermission>();

    }
}
