﻿using System;

namespace UserManagementService.Models
{
    public class GroupRole
    {
        public int GroupId { get; set; }
        public Group Group { get; set; }

        public Guid RoleId { get; set; }
        public Role Role { get; set; }
    }

    public class UserGroup
    {
        public Guid UserId { get; set; }
        public User User { get; set; }

        public int GroupId { get; set; }
        public Group Group { get; set; }
    }

    public class ApiTokenPermission
    {
        public string Token { get; set; }
        public ApiToken ApiToken { get; set; }

        public int PermissionId { get; set; }
        public Permission Permission { get; set; }
    }
}