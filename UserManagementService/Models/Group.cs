﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace UserManagementService.Models
{
    public class Group
    {
        [Key]
        public int GroupId { get; set; }

        [Required]
        public string GroupName { get; set; }

        public ICollection<GroupRole> GroupRoles { get; set; } = new List<GroupRole>();
        public ICollection<UserGroup> GroupUsers { get; set; } = new List<UserGroup>();

        public void GrantRole(Role role)
        {
            GroupRoles.Add(new GroupRole { Group = this, Role = role });
        }
    }
}
