﻿using Microsoft.AspNetCore.Identity;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace UserManagementService.Models
{
    public class Role : IdentityRole
    {
        // Required for many-to-many relation from EF Core
        //public ICollection<GroupRole> Groups { get; set; } = new List<GroupRole>();

        public Role() { }

        public Role(string roleName) : base(roleName) { }
    }
}
