﻿using Microsoft.AspNetCore.Cryptography.KeyDerivation;
using Microsoft.AspNetCore.Identity;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Security.Cryptography;
using System.Threading.Tasks;

namespace UserManagementService.Models
{
    public class User : IdentityUser
    {
        //public ICollection<UserGroup> UserGroups { get; set; } = new List<UserGroup>();

        public User() { }

        public User(string userName) : base(userName) { }

        //public void GrantMembership(Group group)
        //{
        //    UserGroups.Add(new UserGroup { User = this, Group = group });
        //}
    }
}
