﻿using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Hosting.Server.Features;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.Logging;
using Newtonsoft.Json.Linq;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Http;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading;
using System.Threading.Tasks;
using UserManagementService.Interfaces;

namespace UserManagementService.Services
{
    public class UserManagementAnnouncementService : IUserManagementAnnouncementService
    {
        private readonly IConfiguration Configuration;
        private readonly ILogger Logger;

        private readonly string ListenHost;
        private readonly ushort ListenPort;
        private readonly string SharedSecret;

        private HttpClient HttpClient { get; set; }

        public UserManagementAnnouncementService(ILogger<UserManagementAnnouncementService> logger, IConfiguration configuration)
        {
            Configuration = configuration;
            Logger = logger;
            HttpClient = new HttpClient { BaseAddress = new Uri("http://localhost:55124") };

            var listenUrl = Configuration.GetValue<string>("URLS");
            var uri = new Uri(listenUrl);
            ListenHost = uri.Host;
            ListenPort = (ushort)uri.Port;
            SharedSecret = Configuration.GetValue<string>("UserManagementSharedSecret");
        }

        public async Task RegisterAsync()
        {
            var wait = 0;
            bool success;

            do
            {
                if (wait > 0) Logger.LogInformation($"Waiting {wait / 1000} seconds to retry connecting.");
                Thread.Sleep(wait);
                success = await TryRegisterAsync();
                wait = wait > 0 ? Math.Min(wait * 2, 60_000) : 1_000;
            } while (!success);

            Logger.LogInformation("Registered as a User Management Service");
        }

        protected async Task<bool> TryRegisterAsync()
        {
            var jobject = new JObject
                    {
                        { "host", JToken.FromObject(ListenHost) },
                        { "port", JToken.FromObject(ListenPort) }
                    };
            var stringData = jobject.ToString(Newtonsoft.Json.Formatting.None);
            var contentData = new StringContent(stringData, Encoding.UTF8, "application/json");

            try
            {
                var result = await HttpClient.PostAsync($"/api/UserManagement/{SharedSecret}", contentData);
                if (result.IsSuccessStatusCode) return true;
                else if ((int)result.StatusCode >= 400 && (int)result.StatusCode < 500)
                {
                    throw new InvalidOperationException($"Unable to register with User Gateway. Reason: {(int)result.StatusCode}: {result.ReasonPhrase}");
                }
                else
                {
                    Logger.LogInformation($"Unable to register. Reason: {(int)result.StatusCode}: {result.ReasonPhrase}");
                }
            }
            catch (HttpRequestException ex)
            {
                Logger.LogInformation($"Unable to register. Reason {ex.Message}");
                return false;
            }
            return false;
        }
    }
}
