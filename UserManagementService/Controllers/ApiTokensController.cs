﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using UserManagementService.Data;
using UserManagementService.Models;

namespace UserManagementService.Controllers
{
    [Produces("application/json")]
    [Route("api/ApiTokens")]
    public class ApiTokensController : Controller
    {
        private readonly UserManagementContext _context;

        public ApiTokensController(UserManagementContext context)
        {
            _context = context;
        }

        // GET: api/ApiTokens/Validate/{token}/{permission}
        [HttpGet("Validate/{apiToken}/{permission}")]
        public async Task<IActionResult> ValidateToken([FromRoute] string apiToken, [FromRoute] string permission)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            var token = await _context.ApiTokens.SingleOrDefaultAsync(t => t.Token == apiToken);
            if (token != default(ApiToken))
            {
                var perm = token.Permissions.SingleOrDefault(p => p.Permission.PermissionName == permission);
                if (perm.Permission == default(Permission))
                {
                    return Ok(true);
                }
                else
                {
                    return Ok(false);
                }
            }
            else return NotFound(apiToken);
        }

        // POST: api/ApiTokens/Create
        [HttpPost("Create")]
        public IActionResult CreateToken([FromBody] string[] permissions)
        {
            if (!ModelState.IsValid) return BadRequest();
            var user = Request.HttpContext.User;
            return Ok();
        }
    }
}