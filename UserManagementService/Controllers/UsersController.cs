﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Security.Claims;
using System.Threading.Tasks;
using IdentityServer4;
using IdentityServer4.Models;
using IdentityServer4.Services;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Identity;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Logging;
using UserManagementService.Data;
using UserManagementService.Models;

namespace UserManagementService.Controllers
{
    [Produces("application/json")]
    [Route("api/Users")]
    [Authorize]
    public class UsersController : Controller
    {
        private readonly UserManagementContext _context;
        private readonly ILogger<UsersController> Logger;
        private readonly SignInManager<User> SignInManager;
        private readonly UserManager<User> UserManager;
        private readonly IdentityServerTools IdentityServerTool;
        private readonly IIdentityServerInteractionService interactions;

        public UsersController(UserManagementContext context,
            ILogger<UsersController> logger,
            SignInManager<User> signInManager,
            UserManager<User> userManager,
            IdentityServerTools identityServerTool,
            IIdentityServerInteractionService interactions)
        {
            _context = context;
            Logger = logger;
            SignInManager = signInManager;
            UserManager = userManager;
            this.IdentityServerTool = identityServerTool;
            this.interactions = interactions;
        }

        // GET: api/Users
        [HttpGet]
        public IEnumerable<User> GetUsers()
        {
            return _context.Users;
        }

        // GET: api/Users/5
        [HttpGet("{id}")]
        public async Task<IActionResult> GetUser([FromRoute] string id)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            var user = await UserManager.FindByIdAsync(id);

            if (user == null)
            {
                return NotFound();
            }

            return Ok(user);
        }

        // PUT: api/Users/5
        [HttpPut("{id}")]
        public async Task<IActionResult> PutUser([FromRoute] string id, [FromBody] User user)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            if (id != user.Id)
            {
                return BadRequest();
            }

            _context.Entry(user).State = EntityState.Modified;

            try
            {
                await _context.SaveChangesAsync();
            }
            catch (DbUpdateConcurrencyException)
            {
                if (!UserExists(id))
                {
                    return NotFound();
                }
                else
                {
                    throw;
                }
            }

            return NoContent();
        }


        // DELETE: api/Users/5
        [HttpDelete("{id}")]
        public async Task<IActionResult> DeleteUser([FromRoute] string id)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            var user = await UserManager.FindByIdAsync(id);
            if (user == null)
            {
                return NotFound();
            }

            await UserManager.DeleteAsync(user);

            return Ok(user);
        }

        [HttpPost("Create/{userName}")]
        public async Task<IActionResult> CreateUser([FromRoute] string userName, [FromBody] string password)
        {
            if (!ModelState.IsValid || userName == null || password == null) return BadRequest();

            var user = new User(userName);
            if (UserManager.Users.Any(u => u.UserName.ToLower() == user.UserName.ToLower())) return StatusCode(409); // Already exists 

            var result = await UserManager.CreateAsync(user, password);
            if (result.Succeeded) return Ok(user);
            else return StatusCode(500);
        }

        [HttpPost("Login/{userName}")]
        [AllowAnonymous]
        public async Task<IActionResult> Login([FromRoute] string userName, [FromBody] string password)
        {
            if (!ModelState.IsValid) return BadRequest();

            var user = await UserManager.FindByNameAsync(userName);
            if (user == null) return BadRequest();

            var result = await SignInManager.PasswordSignInAsync(user, password, false, false);
            if (result.Succeeded)
            {
                var roles = await UserManager.GetRolesAsync(user);
                var roleClaims = roles.Select(role => new Claim(ClaimTypes.Role, role));
                var token = IdentityServerTool.IssueClientJwtAsync("UserGateway", 3600, audiences: new List<string> { "http://localhost:55124" });

                return Ok(await token);
            }
            else return BadRequest();
        }

        [HttpPost("Logout/")]
        public async Task<IActionResult> Logout()
        {
            if (!ModelState.IsValid) return BadRequest();

            await SignInManager.SignOutAsync();
            return Ok();
        }

        //[HttpGet]
        //public async Task<IActionResult> ValidateUser()
        //{
        //    SignInManager.
        //}

        private bool UserExists(string id)
        {
            return _context.Users.Any(e => e.Id == id);
        }
    }
}