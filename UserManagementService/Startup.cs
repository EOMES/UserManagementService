﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Http;
using System.Text;
using System.Threading.Tasks;
using EventBusClient;
using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Identity;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Logging;
using Microsoft.Extensions.Options;
using Microsoft.EntityFrameworkCore;
using Newtonsoft.Json.Linq;

using Pomelo.EntityFrameworkCore.MySql;
using ServiceTypes;
using ServiceUtilities;
using UserManagementService.Data;
using UserManagementService.Interfaces;
using UserManagementService.Services;
using UserManagementService.Models;
using IdentityServer4.Models;

namespace UserManagementService
{
    public class Startup
    {
        private readonly ILogger Logger;

        public Startup(IConfiguration configuration, ILogger<Startup> logger)
        {
            Configuration = configuration;
            Logger = logger;
        }

        public IConfiguration Configuration { get; }

        // This method gets called by the runtime. Use this method to add services to the container.
        public void ConfigureServices(IServiceCollection services)
        {
            services.AddMvc();

            services.AddDbContext<UserManagementContext>(options => options.UseMySql(Configuration.GetConnectionString("XomesDatabase")));

            services.AddIdentity<Models.User, IdentityRole>()
                .AddEntityFrameworkStores<UserManagementContext>()
                .AddDefaultTokenProviders();

            services.Configure<IdentityOptions>(options =>
            {
                options.Password.RequiredLength = 4;
                options.Password.RequireNonAlphanumeric = false;
                options.Password.RequireDigit = false;
                options.Password.RequireLowercase = false;
                options.Password.RequireUppercase = false;
            });

            //services.ConfigureApplicationCookie(options =>
            //{
            //    options.Events.OnRedirectToLogin = ((o) =>
            //    {
            //        o.Response.StatusCode = StatusCodes.Status401Unauthorized;
            //        return Task.CompletedTask;
            //    });
            //});

            services.AddIdentityServer()
                .AddDeveloperSigningCredential()
                .AddInMemoryClients(new List<Client>
                {
                    new Client
                    {
                        ClientId = "userGateway",
                        ClientName = "UserGateway",
                        ClientSecrets = { new Secret("secret".Sha256()) },
                        AllowedGrantTypes = GrantTypes.ResourceOwnerPassword,
                        AllowedScopes = { "users" },
                        AlwaysSendClientClaims = true,
                    }
                })
                .AddInMemoryApiResources(new List<ApiResource>
                {
                    new ApiResource("users", "Users")
                })
                .AddAspNetIdentity<User>();

            services.AddSingleton<IEventBusClient, EventBusClient.EventBusClient>();
            services.AddSingleton<IServiceClient, ServiceClient>();
            services.AddSingleton<IUserManagementAnnouncementService, UserManagementAnnouncementService>();
        }

        // This method gets called by the runtime. Use this method to configure the HTTP request pipeline.
        public void Configure(IApplicationBuilder app, IApplicationLifetime lifetime, IHostingEnvironment env)
        {
            if (env.IsDevelopment())
            {
                app.UseDeveloperExceptionPage();
            }

            //app.UseAuthentication();
            app.UseIdentityServer();

            app.UseMvc();

            lifetime.ApplicationStarted.Register(ApplicationStarted, app.ApplicationServices);
            lifetime.ApplicationStopping.Register(ApplicationStopping, app.ApplicationServices);
        }

        private async void ApplicationStarted(object appServices)
        {
            if (appServices is IServiceProvider applicationServices)
            {
                Logger.LogInformation("Announcing as User Manager");
                var announcer = applicationServices.GetService<IUserManagementAnnouncementService>();
                await announcer.RegisterAsync();

                // Register as a service
                Logger.LogInformation("Registering as a service");
                var serviceClient = applicationServices.GetService<IServiceClient>();
                serviceClient.ApiKey = "";
                serviceClient.GatewayHost = "localhost";
                serviceClient.GatewayPort = 55124;
                serviceClient.ServiceDisplayName = "Simple User Management Service";
                serviceClient.ServiceName = "simpleUserMgmnt";
                serviceClient.ServiceType = (int)ServiceType.UserManagement;
                serviceClient.UseHttps = false;
                await serviceClient.ConnectAsync();

                // Connect to event bus
                Logger.LogInformation("Connecting to Event Bus");
                var eventBusClient = applicationServices.GetService<IEventBusClient>();
                eventBusClient.AuthorizationToken = "";
                eventBusClient.Host = "localhost";
                eventBusClient.Port = 55124;
                eventBusClient.ServiceUuid = serviceClient.ServiceUuid;
                await eventBusClient.ConnectAsync();
                await eventBusClient.SubscribeAsync(1000);
            }
        }

        private void ApplicationStopping(object appServices)
        {
            if (appServices is IServiceProvider applicationServices)
            {
                var eventBusClient = applicationServices.GetService<IEventBusClient>();
                // TODO: Announce shutting down
                eventBusClient.CloseAsync();
            }
        }
    }
}
