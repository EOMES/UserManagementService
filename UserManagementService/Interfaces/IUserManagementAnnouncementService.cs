﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace UserManagementService.Interfaces
{
    public interface IUserManagementAnnouncementService
    {
        Task RegisterAsync();
    }
}
